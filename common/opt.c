/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2020-2022 Brett Sheffield <bacs@librecast.net> */

#define _GNU_SOURCE /* reallocarray() */
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include "log.h"
#include "opt.h"

char *progname;

int opt_set_str(opt_t *opt, int *argc, char **argv[])
{
	(*argc)--;
	if (!*argc) return -1;
	*opt->var_str = *(++(*argv));
	return 0;
}

int opt_set_stra(opt_t *opt, int *argc, char **argv[])
{
	int len = *opt->len;
	(*argc)--;
	if (!*argc) return -1;
	*opt->var_stra = reallocarray(*opt->var_stra, len + 1, sizeof(char *));
	DEBUG("len=%i %p", len, (void *)*opt->var_stra);
	(*opt->var_stra)[len] = *(++(*argv));
	*opt->len = len + 1;
	return 0;
}

int opt_set_int(opt_t *opt, int *argc, char **argv[])
{
	(*argc)--;
	if (!*argc) return -1;
	switch (opt->type) {
	case OTYPE_DOUBLE:
		*opt->var_double = strtod(*(++(*argv)), NULL);
		break;
	case OTYPE_FLOAT:
		*opt->var_float = strtof(*(++(*argv)), NULL);
		break;
	case OTYPE_LONG:
		*opt->var_int = atol(*(++(*argv)));
		break;
	case OTYPE_INT:
		*opt->var_int = atoi(*(++(*argv)));
		break;
	case OTYPE_SHORT:
		*opt->var_short = atoi(*(++(*argv)));
		break;
	default:
		break;
	}
	return 0;
}

int opt_set_true(opt_t *opt)
{
	*opt->var_bool = 1;
	return 0;
}

static int opt_longoptcmp(char *olong, const char *arg)
{
	if (!strncmp(arg, "--", 2)) {
		if  (!strcmp(arg+2, olong)) {
			return 1;
		}
	}
	return 0;
}

static int opt_valid(opt_parser_t *parser, const char *arg, opt_t **opt)
{
	for (int i = 0; i < parser->optc; i++) {
		*opt = &parser->optv[i];
		if (arg[0] == '-') {
			if (arg[1] == (*opt)->oshort)
				return 1;
			if (opt_longoptcmp((*opt)->olong, arg))
				return 1;
		}
	}
	return 0;
}

int opt_parse(opt_parser_t *parser, int *argc, char **argv[])
{
	int rc = 0;
	opt_t *opt;
	progname = *(*argv)++;
	(*argc)--;
	while ((*argc) && **argv) {
		DEBUG("parsing '%s'", *argv[0]);
		if (**argv[0] != '-') return 0; /* nec tamen consumebatur! */
		if (!opt_valid(parser, *argv[0], &opt)) return -1;
		if (opt->f != NULL) {
			opt->f(opt, argc, argv);
		}
		else {
			switch (opt->type) {
			case OTYPE_BOOL:
				if ((rc = opt_set_true(opt)))
					return rc;
				break;
			case OTYPE_DOUBLE:
			case OTYPE_FLOAT:
			case OTYPE_LONG:
			case OTYPE_INT:
			case OTYPE_SHORT:
				if ((rc = opt_set_int(opt, argc, argv)))
					return rc;
				break;
			case OTYPE_STR:
				if ((rc = opt_set_str(opt, argc, argv)))
					return rc;
				break;
			case OTYPE_STRA:
				if ((rc = opt_set_stra(opt, argc, argv)))
					return rc;
				break;
			}
		}
		(*argc)--;
		(*argv)++;
	}
	return 0;
}

int opt_new(opt_parser_t *parser, opt_t *opt)
{
	/* assert() - I never check the return value of this function */
	assert(parser->last < parser->optc); /* call to opt_init() needs updating */
	if (parser->last < parser->optc) {
		parser->optv[parser->last++] = *opt;
		return 0;
	}
	return -1;
}

void opt_free(opt_parser_t *parser)
{
	for (int i = 0; i < parser->optc; i++) {
		if (parser->optv[i].len) free(*parser->optv[i].var_stra);
	}
	free(parser);
}

opt_parser_t *opt_init(int optc)
{
	opt_parser_t *parser;
	if (!optc || !(parser = calloc(1, sizeof(opt_parser_t) + sizeof(opt_t) * optc)))
		return NULL;
	parser->optc = optc;
	return parser;
}
