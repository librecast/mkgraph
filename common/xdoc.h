/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2020-2022 Brett Sheffield <bacs@librecast.net> */

#ifndef _XDOC_H
#define _XDOC_H 1

#define XDOC_SELFCLOSE 1
#define XDOC_NOCLOSE 2
#define XDOC_DOCOPEN 4
#define XDOC_DOCCLOSE 8

typedef struct xattr_s xattr_t;
typedef struct xelement_s xelement_t;
typedef struct xdoc_s xdoc_t;

struct xattr_s {
	char *key;
	char *val;
	xattr_t *next;
	xattr_t *prev;
};

struct xelement_s {
	char *name;
	char *text;
	int flags;
	xattr_t *attr;
	xelement_t *parent;
	xelement_t *child;
	xelement_t *next;
	xelement_t *prev;
};

struct xdoc_s {
	char *doctype;
	xelement_t *root;
};

void xdoc_render_attrs(int fd, xattr_t *attr);
void xdoc_render_nodes(int fd, xelement_t *node, int flags);
void xdoc_render(int fd, xdoc_t *doc);
xelement_t * xdoc_element_child(xelement_t *parent, xelement_t *node);
xelement_t * xdoc_element_append(xelement_t *prev, xelement_t *node);
xelement_t * xdoc_attr_append(xelement_t *node, xattr_t *attr);
xelement_t *xdoc_element_new(char *name, char *text, int flags);
xelement_t *xdoc_selfclose(xelement_t *node);
xattr_t *xdoc_attr_new(char *key, char *val, ...);
void xdoc_element_free(xelement_t *node);
void xdoc_attr_free(xattr_t *attr);
xattr_t *xdoc_attr_set(xattr_t *attr, char *key, char *val, ...);
xattr_t *xdoc_attr_vset(xattr_t *attr, char *key, char *val, va_list argp);
xattr_t *xdoc_element_attr(xelement_t *node, char *key, char *val, ...);
void xdoc_dump(int fd, xelement_t *node);
void xdoc_dump_open(int fd, xelement_t *node);
void xdoc_dump_close(int fd, xelement_t *node);

#endif /* _XDOC_H */
