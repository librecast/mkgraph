/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2020-2022 Brett Sheffield <bacs@librecast.net> */

#ifndef _OPT_H
#define _OPT_H

enum opt_type {
	OTYPE_BOOL,
	OTYPE_SHORT,
	OTYPE_INT,
	OTYPE_LONG,
	OTYPE_FLOAT,
	OTYPE_DOUBLE,
	OTYPE_STR,
	OTYPE_STRA
};

typedef struct opt_s opt_t;
struct opt_s {
	union {
		char ***var_stra;
		char **var_str;
		double *var_double;
		float *var_float;
		long *var_long;
		int *var_int;
		short *var_short;
		int *var_bool;
	};
	int	*len; /* counter for arrays */
	char	oshort;
	char	olong[32];
	char	help[128];
	enum opt_type type;
	int (*f)(opt_t *opt, int *argc, char **argv[]);
};

typedef struct opt_parser_s {
	int     optc;
	int     last;
	opt_t   optv[];
} opt_parser_t;

extern char *progname;

/* initialise option parser with opts options */
opt_parser_t *opt_init(int opts);

/* free parser */
void opt_free(opt_parser_t *parser);

/* register new option with parser */
int opt_new(opt_parser_t *parser, opt_t *opt);

/* parse option arguments */
int opt_parse(opt_parser_t *parser, int *argc, char **argv[]);

/* set boolean (int) to true (1) */
int opt_set_true(opt_t *opt);

/* set integer value */
int opt_set_int(opt_t *opt, int *argc, char **argv[]);

/* set string value */
int opt_set_str(opt_t *opt, int *argc, char **argv[]);

/* set multiple string array value */
int opt_set_stra(opt_t *opt, int *argc, char **argv[]);

#endif /* _OPT_H */
