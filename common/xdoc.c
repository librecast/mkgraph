/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2020-2022 Brett Sheffield <bacs@librecast.net> */

/* DOM rendering code */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include "xdoc.h"

void xdoc_render_attrs(int fd, xattr_t *attr)
{
	while (attr) {
		if (attr->key) dprintf(fd, " %s", attr->key);
		if (attr->val) dprintf(fd, "=\"%s\"", attr->val);
		attr = attr->next;
	}
}

void xdoc_render_nodes(int fd, xelement_t *node, int flags)
{
	while (node) {
		int selfclose = ((node->flags & XDOC_SELFCLOSE) == XDOC_SELFCLOSE);
		int docopen = ((flags & XDOC_DOCOPEN) == XDOC_DOCOPEN);
		int docclose = ((flags & XDOC_DOCCLOSE) == XDOC_DOCCLOSE);
		if (node->name && !docclose) dprintf(fd, "<%s", node->name);
		if (!docclose) xdoc_render_attrs(fd, node->attr);
		if (node->name && !selfclose && !docclose) dprintf(fd, ">");
		if (node->text) dprintf(fd, "%s", node->text);
		else if (!selfclose && !docclose) dprintf(fd, "\n");
		xdoc_render_nodes(fd, node->child, flags);
		if (node->name && !docopen && ((node->flags & XDOC_NOCLOSE) != XDOC_NOCLOSE)) {
			if (selfclose) dprintf(fd, "/>\n");
			else dprintf(fd, "</%s>\n", node->name);
		}
		node = node->next;
	}
}

void xdoc_render(int fd, xdoc_t *doc)
{
	if (doc->doctype) dprintf(fd, "<!DOCTYPE %s>\n", doc->doctype);
	xdoc_render_nodes(fd, doc->root, 0);
}

xelement_t * xdoc_element_child(xelement_t *parent, xelement_t *node)
{
	node->parent = parent;
	xelement_t *sib = parent->child;
	while (sib && sib->next) sib = sib->next;
	if (sib) {
		node->prev = sib;
		sib->next = node;
	}
	else parent->child = node;
	return node;
}

xelement_t * xdoc_element_append(xelement_t *prev, xelement_t *node)
{
	node->prev = prev;
	node->parent = prev->parent;
	prev->next = node;
	return node;
}

xelement_t * xdoc_attr_append(xelement_t *node, xattr_t *attr)
{
	xattr_t *sib = node->attr;
	while (sib && sib->next) sib = sib->next;
	if (sib) {
		attr->prev = sib;
		sib->next = attr;
	}
	else node->attr = attr;
	return node;
}

void xdoc_attr_free(xattr_t *attr)
{
	xattr_t *tmp;
	while (attr) {
		tmp = attr;
		free(attr->val);
		attr = attr->next;
		free(tmp);
	}
	free(attr);
}

void xdoc_element_free(xelement_t *node)
{
	xelement_t *tmp;
	while (node) {
		tmp = node;
		xdoc_element_free(node->child);
		xdoc_attr_free(node->attr);
		node = node->next;
		free(tmp);
	}
	free(node);
}

xelement_t *xdoc_element_new(char *name, char *text, int flags)
{
	xelement_t *node = malloc(sizeof(xelement_t));
	memset(node, 0, sizeof(xelement_t));
	node->name = name;
	node->text = text;
	node->flags = flags;
	return node;
}

xattr_t *xdoc_attr_set(xattr_t *attr, char *key, char *val, ...)
{
	int len;
	char *buf;
	va_list argp;
	attr->key = key;
	if (val) {
		va_start(argp, val);
		len = vsnprintf(NULL, 0, val, argp);
		va_end(argp);
		va_start(argp, val);
		buf = malloc(len + 1);
		vsprintf(buf, val, argp);
		va_end(argp);
		attr->val = buf;
	}
	return attr;
}

xattr_t *xdoc_attr_vset(xattr_t *attr, char *key, char *val, va_list argp)
{
	int len;
	char *buf;
	va_list copy;
	attr->key = key;
	if (val) {
		va_copy(copy, argp);
		len = vsnprintf(NULL, 0, val, copy);
		va_end(copy);
		buf = malloc(len + 1);
		va_copy(copy, argp);
		vsprintf(buf, val, copy);
		va_end(copy);
		attr->val = buf;
	}
	return attr;
}

xattr_t *xdoc_attr_vnew(char *key, char *val, va_list argp)
{
	va_list copy;
	xattr_t *attr = malloc(sizeof(xattr_t));
	memset(attr, 0, sizeof(xattr_t));
	if (val) {
		va_copy(copy, argp);
		xdoc_attr_vset(attr, key, val, copy);
		va_end(copy);
	}
	return attr;
}

xattr_t *xdoc_attr_new(char *key, char *val, ...)
{
	va_list argp;
	xattr_t *attr = malloc(sizeof(xattr_t));
	memset(attr, 0, sizeof(xattr_t));
	if (val) {
		va_start(argp, val);
		xdoc_attr_vset(attr, key, val, argp);
		va_end(argp);
	}
	return attr;
}

xattr_t *xdoc_element_attr(xelement_t *node, char *key, char *val, ...)
{
	va_list argp;
	xattr_t *attr = NULL;
	va_start(argp, val);
	attr = xdoc_attr_vnew(key, val, argp);
	va_end(argp);
	xdoc_attr_append(node, attr);
	return attr;
}

/* dump opening tags - don't close */
void xdoc_dump_open(int fd, xelement_t *node)
{
	xdoc_render_nodes(fd, node, XDOC_DOCOPEN);
}

/* dump document closing tags */
void xdoc_dump_close(int fd, xelement_t *node)
{
	xdoc_render_nodes(fd, node, XDOC_DOCCLOSE);
}

/* render and free */
void xdoc_dump(int fd, xelement_t *node)
{
	xdoc_render_nodes(fd, node, 0);
	xdoc_element_free(node);
}

#if 0
int main(int argc, char *argv[])
{
	int fd = STDOUT_FILENO;
	xelement_t html = { .name = "html" };
	xelement_t head = { .name = "head" };
	xattr_t body_lang = { .key = "lang", .val = "fr" };
	xattr_t body_bg = { .key = "background-color", .val = "white" };
	xelement_t body = { .name = "body"};
	xelement_t title = { .name = "title", .text="Xdoc Title" };
	xdoc_t doc = { .doctype = "html", .root = &html };
	xdoc_attr_append(&body, &body_bg);
	xdoc_attr_append(&body, &body_lang);
	xdoc_element_child(&head, &title);
	xdoc_element_child(doc.root, &head);
	xdoc_element_append(doc.root->child, &body);
	xdoc_render(fd, &doc);
	return 0;
}
#endif
