# README - mkgraph

SVG graphing utility.

See man page for documentation.

## LICENSE

GPL2 or GPL3 (at your option)

## INSTALL

`make`

`make install`
