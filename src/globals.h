/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2020-2022 Brett Sheffield <bacs@librecast.net> */

#ifndef _GLOBALS_H
#define _GLOBALS_H 1

#include "opt.h"

extern opt_parser_t *parser;
extern int verbose;
extern int quiet;
extern int xtime; /* x-axis time series */
extern char *title;
extern char *gridcolor;
extern char *titlecolor;
extern char *bgcolor;
extern char *bgcolorinner;
extern char *fgcolor;
extern char *bordercolor;
extern char *outfile;
extern char *color_drop;
extern char *color_fail;
extern char *color_missing;
extern char *color_null;
extern char **colorv;
extern int colorc;
extern char *g_default_color[];
extern int g_default_color_len;
extern char **labelv;
extern int labelc;
extern char **unitv;
extern int unitc;
extern char **scalev;
extern int scalec;
extern char **ymaxv;
extern int ymaxc;
extern char **plotv; /* plot type "point", "ibar", "cendle" */
extern int plotc;
extern long tstart;
extern long tend;
extern long tival;
extern int thours;
extern long height;
extern long width;
extern long radius;
extern long fieldc;
extern int key_h;
extern int key_w;
extern float x_step;
extern float x_max;
extern long y_min;
extern long y_max;
extern long gridh;
extern long gridv;
extern int top;

#endif /* _GLOBALS_h */
