/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2020-2022 Brett Sheffield <bacs@librecast.net> */

#include <assert.h>
#include <float.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "globals.h"
#include "arg.h"
#include "log.h"
#include "xdoc.h"

static char *getseriescolor(int i)
{
	return (i < colorc) ? colorv[i] : g_default_color[i % g_default_color_len];
}

/* scan input file and determine min/max data values */
void getminmax(FILE *f, long *x_min, long *x_max, long *y_min, long *y_max, long *line)
{
	int ts, code, ms, byt, speed;
	*line = 0;
	while (fscanf(f, "%i %i %i %i %i", &ts, &code, &ms, &byt, &speed) == 5) {
		if (!*line) {
			*x_min = ts;
			*x_max = ts;
			*y_min = ms;
			*y_max = ms;
		}
		else {
			if (ts < *x_min) *x_min = ts;
			if (ts > *x_max) *x_max = ts;
			if (ms < *y_min) *y_min = ms;
			if (ms > *y_max) *y_max = ms;
		}
		(*line)++;
	}
}

/* draw vertical line */
static void vline(int fd, float x, float y, int height, char *color)
{
	xelement_t *node;
	node = xdoc_element_new("path", NULL, XDOC_SELFCLOSE);
	xdoc_element_attr(node, "fill", color);
	xdoc_element_attr(node, "stroke", color);
	xdoc_element_attr(node, "d", "M %g %g v %i", x, y, height);
	xdoc_dump(fd, node);
}


static inline void drawgrid(int fd, float x, int y, int height, int width)
{
	xelement_t *node;
	for (int i = 0; gridh && i < height; i += gridh) { /* horizontal lines */
		node = xdoc_element_new("path", NULL, XDOC_SELFCLOSE);
		xdoc_element_attr(node, "class", "gridline");
		xdoc_element_attr(node, "stroke-width", "0.5");
		xdoc_element_attr(node, "fill", "transparent");
		xdoc_element_attr(node, "stroke", "#808080");
		xdoc_element_attr(node, "d", "M %g %i h %i", x, y + i, width);
		xdoc_dump(fd, node);
	}
	for (int i = 0; gridv && i < width; i += gridv) { /* vertical lines */
		node = xdoc_element_new("path", NULL, XDOC_SELFCLOSE);
		xdoc_element_attr(node, "class", "gridline");
		xdoc_element_attr(node, "stroke-width", "0.5");
		xdoc_element_attr(node, "fill", "transparent");
		xdoc_element_attr(node, "stroke", "#808080");
		xdoc_element_attr(node, "d", "M %g %i v %i", x + i, y, height);
		xdoc_dump(fd, node);
	}
}

static void drawtext(int fd, float x, int y, char *text, char *fill)
{
	xelement_t *node;
	node = xdoc_element_new("text", text, 0);
	xdoc_element_attr(node, "fill", fill);
	xdoc_element_attr(node, "font-family", "monospace");
	xdoc_element_attr(node, "font-weight", "bold");
	xdoc_element_attr(node, "font-size", "10");
	xdoc_element_attr(node, "dominant-baseline", "middle");
	xdoc_element_attr(node, "x", "%g", x);
	xdoc_element_attr(node, "y", "%i", y);
	xdoc_dump(fd, node);
}

static void drawtitle(int fd)
{
	xelement_t *node;
	node = xdoc_element_new("text", title, 0);
	xdoc_element_attr(node, "class", "graphtitle");
	xdoc_element_attr(node, "fill", "gray");
	xdoc_element_attr(node, "font-family", "Verdana");
	xdoc_element_attr(node, "font-size", "18");
	xdoc_element_attr(node, "text-anchor", "left");
	xdoc_element_attr(node, "dominant-baseline", "middle");
	xdoc_element_attr(node, "x", "10");
	xdoc_element_attr(node, "y", "12%%");
	xdoc_dump(fd, node);
}

static void drawrect(int fd, float x, int y, int h, int w, float thick, char *colorfill, char *colorline, float opacity)
{
	xelement_t *node;
	node = xdoc_element_new("path", NULL, XDOC_SELFCLOSE);
	xdoc_element_attr(node, "stroke-width", "%g", thick);
	xdoc_element_attr(node, "fill-opacity", "%g", opacity);
	xdoc_element_attr(node, "fill", colorfill);
	xdoc_element_attr(node, "stroke", colorline);
	xdoc_element_attr(node, "d", "M %g %i h %i v %i h %i v %i", x, y, w, h, -w, -h);
	xdoc_dump(fd, node);
}


static void drawsquare(int fd, float x, int y, int s, int thick, char *colorfill, char *colorline, float opacity)
{
	drawrect(fd, x, y, s, s, thick, colorfill, colorline, opacity);
}

static void drawkeyitem(int fd, float x, int y)
{
	int s = 10; /* side len */
	float thick = 1;

	for (int i = 0; i < labelc; i++) {
		drawsquare(fd, x, y, s, thick, getseriescolor(i), "black", 1.0f);
		drawtext(fd, x + 15, y + s / 2, labelv[i], "black");
		y += 15;
	}
}

static void drawscale_x(int fd)
{
	xelement_t *node;
	float x_min = 0;
	char text[32];
	int lpix = 50; /* pixels required to display one x value */
	int xvals = width / lpix; /* number of x values to show */
	int step = (x_max - x_min) / xvals;

	for (float x = x_min; x <= x_max; x += step) {
		snprintf(text, 32, "%i", (int) x);
		node = xdoc_element_new("text", text, 0);
		xdoc_element_attr(node, "fill", "black");
		xdoc_element_attr(node, "font-family", "monospace");
		xdoc_element_attr(node, "font-weight", "bold");
		xdoc_element_attr(node, "font-size", "10");
		xdoc_element_attr(node, "text-anchor", "middle");
		xdoc_element_attr(node, "dominant-baseline", "middle");
		xdoc_element_attr(node, "x", "%g", width / (x_max - x_min) * x);
		xdoc_element_attr(node, "y", "%i", height + 5 + top);
		xdoc_dump(fd, node);
	}
}

static void drawscale(int fd)
{
	enum { bufsz = 48 };
	xelement_t *node, *g;
	char text[bufsz];
	float scale = 1;
	long xoff = width + 15;

	for (int i = 0; i < ymaxc; i++) {

		/* max */
		g = xdoc_element_new("g", NULL, 0);
		xdoc_element_attr(g, "transform", "translate(%i,%i)", xoff, top);
		scale = strtof(ymaxv[i], NULL);
		snprintf(text, bufsz, "%g", scale);
		node = xdoc_element_new("text", text, 0);
		xdoc_element_attr(node, "class", "scale");
		xdoc_element_attr(node, "font-family", "monospace");
		xdoc_element_attr(node, "font-weight", "bold");
		xdoc_element_attr(node, "font-size", "10");
		xdoc_element_attr(node, "text-anchor", "end");
		xdoc_element_attr(node, "transform", "rotate(-90)");
		xdoc_element_child(g, node);
		xdoc_dump(fd, g);

		vline(fd, xoff - 2, top + (height / 5 / 2), height / 5 * 4, fgcolor);

		/* min */
		snprintf(text, bufsz, "%g", y_min * scale);
		g = xdoc_element_new("g", NULL, 0);
		xdoc_element_attr(g, "transform", "translate(%i,%i)", xoff - 3, height + top + 3);
		node = xdoc_element_new("text", text, 0);
		xdoc_element_attr(node, "class", "scale min");
		xdoc_element_attr(node, "font-family", "monospace");
		xdoc_element_attr(node, "font-weight", "bold");
		xdoc_element_attr(node, "font-size", "10");
		xdoc_element_attr(node, "text-anchor", "start");
		xdoc_element_attr(node, "dominant-baseline", "middle");
		xdoc_element_attr(node, "transform", "rotate(-90)");
		xdoc_element_child(g, node);
		xdoc_dump(fd, g);

		/* units */
		if (i < unitc) {
			g = xdoc_element_new("g", NULL, 0);
			xdoc_element_attr(g, "transform", "translate(%i,%i)", xoff - 4, top + height / 2);
			node = xdoc_element_new("text", unitv[i], 0);
			xdoc_element_attr(node, "class", "scale");
			xdoc_element_attr(node, "font-family", "monospace");
			xdoc_element_attr(node, "font-weight", "bold");
			xdoc_element_attr(node, "font-size", "8");
			xdoc_element_attr(node, "text-anchor", "middle");
			xdoc_element_attr(node, "transform", "rotate(-90)");
			xdoc_element_child(g, node);
			xdoc_dump(fd, g);
		}

		xoff += 10;
	}
}

static void drawtime(int fd, int height, int width)
{
	xelement_t *node;
	char text[3];
	struct tm *ttm = localtime((time_t *)&tstart);
	int h = ttm->tm_hour + 1;
	int m = ttm->tm_min;
	int w = (key_w) ? width : width - 8; /* avoid plotting off edge if no key */

	for (float x = (60 * 60 / tival - m) * x_step; x <= w; x += width / thours) {
		if (h == 24) h = 0;
		snprintf(text, 3, "%02i", h);
		node = xdoc_element_new("text", text, 0);
		xdoc_element_attr(node, "fill", "black");
		xdoc_element_attr(node, "font-family", "monospace");
		xdoc_element_attr(node, "font-weight", "bold");
		xdoc_element_attr(node, "font-size", "10");
		xdoc_element_attr(node, "text-anchor", "middle");
		xdoc_element_attr(node, "dominant-baseline", "middle");
		xdoc_element_attr(node, "x", "%g", x);
		xdoc_element_attr(node, "y", "%i", height + 5 + top);
		xdoc_dump(fd, node);
		h++;
	}
}

static void plot_point(int fd, float x, int height, float val, float y_step, float r, char *color, int fld)
{
	xelement_t *node;
	float y;
	float scale = 1;

	if (scalec > fld) {
		scale = strtof(scalev[fld], NULL);
		if (!scale) scale = 1;
	}
	y = (float)height - (val * scale * y_step);
	if (y < 0) {
		y = 0;
		color = "red";
		r=2;
	}
	dprintf(fd, "<!-- x = %f, val = %f -->\n", x, val);
	node = xdoc_element_new("circle", NULL, XDOC_SELFCLOSE);
	xdoc_element_attr(node, "cx", "%g", x);
	xdoc_element_attr(node, "cy", "%g", y + top);
	xdoc_element_attr(node, "r", "%g", r);
	xdoc_element_attr(node, "fill", color);
	xdoc_dump(fd, node);
}

static void plot_ibar(int fd, float x, int height, float min, float max, float y_step, float r, char *color, int fld)
{
	xelement_t *node;
	float lo, hi;
	float scale = 1;
	float barw = radius * 2;

	DEBUG("plotting i-bar with min %f max %f", min, max);
	if (min == max) return;

	if (scalec > fld) {
		scale = strtof(scalev[fld], NULL);
		if (!scale) scale = 1;
	}
	lo = (float)height - (min * scale * y_step);
	hi = (float)height - (max * scale * y_step);
	dprintf(fd, "<!-- x = %f, min = %f max = %f y_step = %f lo = %f hi = %f top = %d height = %d scale = %f -->\n", x, min, max, y_step, lo, hi, top, height, scale);

	/* draw base */
	node = xdoc_element_new("path", NULL, XDOC_SELFCLOSE);
	xdoc_element_attr(node, "class", "ibar base");
	xdoc_element_attr(node, "fill", color);
	xdoc_element_attr(node, "stroke", color);
	xdoc_element_attr(node, "stroke-width", "%i", radius / 2);
	xdoc_element_attr(node, "d", "M %g %g h %g", x - barw/2, hi + r + 1, barw);
	xdoc_dump(fd, node);

	/* draw head */
	node = xdoc_element_new("path", NULL, XDOC_SELFCLOSE);
	xdoc_element_attr(node, "class", "ibar base");
	xdoc_element_attr(node, "fill", color);
	xdoc_element_attr(node, "stroke", color);
	xdoc_element_attr(node, "stroke-width", "%i", radius / 2);
	xdoc_element_attr(node, "d", "M %g %g h %g", x - barw/2, lo + r + 1, barw);
	xdoc_dump(fd, node);

	/* draw vertical */
	node = xdoc_element_new("path", NULL, XDOC_SELFCLOSE);
	xdoc_element_attr(node, "class", "ibar vertical");
	xdoc_element_attr(node, "fill", color);
	xdoc_element_attr(node, "stroke", color);
	xdoc_element_attr(node, "stroke-width", "%i", radius / 2);
	xdoc_element_attr(node, "d", "M %g %g v %i", x, hi + r + 1, (int)lo - (int)hi);
	xdoc_dump(fd, node);
}

int processline(char *line, char **sdata, int max)
{
	int c = 0;
	char *tok = strtok(line, " ");
	while (tok) {
		sdata[c] = tok;
		c++;
		if (c >= max) break;
		tok = strtok(NULL, " ");
	}
	return c;
}

void drawpoints(int fd, FILE *f, int height, int width)
{
	const int maxsamples = 10; /* FIXME  - make option */
	float sdata[maxsamples][fieldc];
	float avg;
	int samples = 0;
	char buf[BUFSIZ];
	char *tok;
	long ts = 0;
	long tnext;
	float ltot;
	float x = 0;
	float y_step = (float)height / (y_max - y_min);
	int fld;
	int lineno = 0;
	int fail = 0;
	int idx, cnt;

	if (!fieldc) fieldc = labelc;

	memset(sdata, 0, sizeof sdata);
	tnext = tstart;
	ts = 0;
	for (x = x_step; x <= width; x += x_step) {
		/* read samples from file until tnext reached */
		while (ts <= tnext && fgets(buf, BUFSIZ, f)) {
			lineno++;
			idx = samples % maxsamples;
			tok = strtok(buf, " "); /* x-axis */
			if (!strcmp(tok, "-")) DEBUG("skipping NULL");
			ts = atol(tok);
			if (ts >= tnext + tival) {
				/* we missed a bit, advance x (gap in x-axis) */
				if (color_drop) vline(fd, x, top, height, color_drop);
				float xo = x;
				while (tnext + tival < ts) {
					x += x_step;
					tnext += tival;
				}
				if (color_missing) drawrect(fd, xo, top, height, x - xo, 0.5f, color_missing, "transparent", 0.4f);
			}

			/* process rest of line (y-axis) */
			tok = strtok(NULL, " ");
			for (fld = 0; fld < fieldc && tok; fld++) {
				if (!strcmp(tok, "-")) {
					sdata[idx][fld] = FLT_MAX;
					continue; /* skip "-" NULL values */
				}
				sdata[idx][fld] = strtof(tok, NULL);
				DEBUG("%i[%i][%i]: sample %f", samples, idx, fld, sdata[idx][fld]);
				tok = strtok(NULL, " ");
			}
			if (fld == fieldc) samples++;

			DEBUG("ts=%li, tnext=%li", ts, tnext);
			if (ts >= tnext) {
				DEBUG("x = %f, ts=%li (plotting)", x, ts);
				/* next x point reached, plot points */
				if (fail && (x > 0) && color_fail)
					vline(fd, x, top, height, color_fail);
				else if (!samples && color_null) /* no samples for this interval */
					vline(fd, x, top, height, color_null);
				else {
					for (fld = 0; fld < fieldc; fld++) {
						char *color = getseriescolor(fld);
						if (ymaxc) {
							int imax = (fld > ymaxc - 1) ? 0 : fld;
							float ff = strtof(ymaxv[imax], NULL);
							y_step = (float)height / (ff - y_min);
						}
						if (plotc - 1 < fld || !strcmp(plotv[fld], "point")) {
							/* calculate mean of samples */
							cnt = 0;
							ltot = 0;
							if (samples > maxsamples) samples = maxsamples;

							for (int i = 0; i < samples; i++) {
								if (sdata[i][fld] != FLT_MAX) {
									ltot += sdata[i][fld];
									cnt++;
								}
							}
							if (!cnt) {
								DEBUG("skipping field - no valid samples");
								continue;
							}
							avg = ltot / cnt;
							dprintf(fd, "<!-- %s -->", labelv[fld]);
							plot_point(fd, x, height, avg, y_step, radius, color, fld);
						}
						else if (fld < plotc - 1 && !strcmp(plotv[fld], "ibar")) {
							float min = 0; // = sdata[0][fld++];
							float max = 0; //sdata[0][fld];
							cnt = 0;
							for (int i = 0; i < samples; i++) {
								if (sdata[i][fld] != FLT_MAX) {
									min += sdata[i][fld];
									max += sdata[i][fld+1];
									cnt++;
								}
							}
							if ((min || max) && min != FLT_MAX && max != FLT_MAX) {
								plot_ibar(fd, x, height, min, max, y_step, radius, color, fld);
							}
							fld++;
						}
					}
				}
				memset(sdata, 0, sizeof sdata);
				samples = 0, fail = 0, idx = 0;
			}
#if 0
			else if (!fail) {
				DEBUG("skipping invalid line %i", lineno);
				vline(fd, x, top, height, "black");
			}
#endif
		}
		tnext += tival;
	}
	DEBUG("all done, x = %g", x);
	if (xtime) drawtime(fd, height, width);
}

static void setstartend(FILE *f)
{
	if (xtime) {
		if (!tend && !tstart) {
			char *tok;
			char buf[BUFSIZ];
			/* get last readable timestamp from file */
			while (fgets(buf, BUFSIZ, f)) {
				tok = strtok(buf, " ");
				if (tok) tend = atol(tok);
			}
			rewind(f);
		}
		else if (!tend) {
			tend = tstart + 1440 * 60;
		}
		if (!tstart) {
			tstart = tend - 1440 * 60;
			tstart /= tival; tstart *= tival; /* align samples to ival boundary */
		}
		if (!width) width = (tend - tstart) / tival;
		thours = (tend - tstart) / 60 / 60;
		x_step = (float)width / (float)tival / ((float)thours);
		DEBUG("start: %li %s", tstart, ctime(&tstart));
		DEBUG("  end: %li %s", tend, ctime(&tend));
		DEBUG("tival: %li", tival);
		DEBUG("thours: %i", thours);
		DEBUG("%li / %li / %i", width, tival, thours);
	}
	else {
		tival = 1;
		if (!width) width = 800;
		if (!height) height = 600;
		x_step = (float)width / (float)tival / x_max;
	}

	DEBUG("h %li x w %li", height, width);
	DEBUG("x_step: %g", x_step);

	//if (x_step <= 0) x_step = 1;
	assert(x_step > 0);
	if (!labelc) key_w = 0; /* no labels, hide key */
}

static void render_node_text(int fo, char *element, const char *template, ...)
{
	va_list argp;
	char *text = NULL;
	size_t sz;
	xelement_t *node;
	va_start(argp, template);
	sz = (size_t) vsnprintf(NULL, 0, template, argp);
	text = malloc(sz);
	va_end(argp);
	assert(text);
	va_start(argp, template);
	vsnprintf(text, sz, template, argp);
	va_end(argp);
	node = xdoc_element_new(element, text, 0);
	xdoc_dump(fo, node);
	free(text);
}

void render_svg(int fo, FILE *fi)
{
	const int out_w = width + key_w;
	const int out_h = height + key_h + top;
	xelement_t *node;
	xelement_t *svg = xdoc_element_new("svg", NULL, 0);

	xdoc_element_attr(svg, "version", "1.0");
	xdoc_element_attr(svg, "baseProfile", "full");
	xdoc_element_attr(svg, "width", "%i", out_w);
	xdoc_element_attr(svg, "height", "%i", out_h);
	xdoc_element_attr(svg, "xmlns", "http://www.w3.org/2000/svg");
	node = xdoc_element_new("g", NULL, 0);
	xdoc_element_child(svg, node);
	xdoc_dump_open(fo, svg);

	render_node_text(fo, "style", ".gridline{stroke:%s;fill:none;;stroke-opacity:0.5;stroke-linecap:square;}", gridcolor);
	render_node_text(fo, "style", "text{fill: %s; };", fgcolor);
	render_node_text(fo, "style", ".graphtitle{fill: %s; };", titlecolor);

	drawrect(fo, 0, 0, out_h, out_w, 1, bgcolor, bordercolor, 1.0f); /* outer box */
	drawrect(fo, 0, top, height, width, 0.5f, bgcolorinner, "#808080", 1.0f); /* graph box */
	drawgrid(fo, 0, top, height, width); /* gridlines */
	if (title) drawtitle(fo);
	drawkeyitem(fo, out_w - 130, height / 3);
	drawscale(fo);
	if (!xtime) drawscale_x(fo);

	/* draw points */
	drawpoints(fo, fi, height, width);
	xdoc_dump_close(fo, svg);
	xdoc_element_free(svg);
}

int main(int argc, char *argv[])
{
	FILE *f_in;
	int fd_out = STDOUT_FILENO, fd_in = STDIN_FILENO;
	int rc = EXIT_FAILURE;
	char *ftmp;

	loginit();
	if ((arg_parse(&argc, &argv))) return EXIT_FAILURE;

	if (argc > 0) {
		DEBUG("reading input from '%s'", argv[0]);
		fd_in = open(argv[0], O_RDONLY);
		if (fd_in == -1) {
			perror("open");
			goto exit_0;
		}
		if (flock(fd_in, LOCK_SH) == -1) {
			perror("fdopen");
			goto exit_1;
		}
	}
	f_in = fdopen(fd_in, "r");
	if (!f_in) {
		perror("fdopen");
		goto exit_2;
	}
	if (outfile) {
		DEBUG("writing output to '%s'", outfile);
		size_t sz = strlen(outfile) + 8;
		ftmp = malloc(sz);
		snprintf(ftmp, sz, "%s-XXXXXX", outfile);
		fd_out = mkstemp(ftmp);
		if (fd_out == -1) {
			perror("open");
			goto exit_3;
		}
	}

	setstartend(f_in);
	render_svg(fd_out, f_in);

	rc = EXIT_SUCCESS;
	if (fd_out != STDOUT_FILENO) {
		fsync(fd_out);
		close(fd_out);
		if ((rename(ftmp, outfile) == -1))
			perror("unable to rename temp file");
		free(ftmp);
	}
exit_3:
	fclose(f_in);
exit_2:
	if (fd_in != STDIN_FILENO) flock(fd_in, LOCK_UN);
exit_1:
	if (fd_in != STDIN_FILENO) close(fd_in);
exit_0:
	opt_free(parser);

	return rc;
}
