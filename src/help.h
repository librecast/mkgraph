/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2020-2022 Brett Sheffield <bacs@librecast.net> */

#ifndef _HELP_H
#define _HELP_H 1

void help_usage(void);

#endif /* _HELP_H */
