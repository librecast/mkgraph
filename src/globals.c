/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2020-2022 Brett Sheffield <bacs@librecast.net> */

#include "globals.h"

opt_parser_t *parser;
int verbose = 0;
int quiet = 0;
int ibar;
int xtime = 0;
char *title;
char *gridcolor = "#808080";
char *titlecolor = "#808080";
char *bgcolor = "white";
char *bgcolorinner = "beige";
char *fgcolor = "black";
char *bordercolor = "gray";
char *outfile;
char *color_drop;
char *color_fail;
char *color_missing;
char *color_null;
char **colorv;
int colorc = 0;
char **labelv;
int labelc = 0;
char **unitv;
int unitc = 0;
char **scalev;
int scalec = 0;
char **ymaxv;
int ymaxc = 0;
char **plotv;
int plotc;
long tstart = 0;
long tend = 0;
long tival = 60;
long width = 0;
long height = 120;
long radius = 1;
int thours = 24;
int key_h = 10;
int key_w = 200;
float x_step = 1;
float x_max = 0;
long y_min = 0;
//long y_max = 2500000;
long y_max = 1;
long gridh = 10;
long gridv = 10;
int top = 5;
long fieldc;

char *g_default_color[] = { "green", "orange", "black", "blue", "teal" };
int g_default_color_len = sizeof g_default_color / sizeof g_default_color[0];
