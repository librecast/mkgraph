/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2020-2022 Brett Sheffield <bacs@librecast.net> */

#include <stdlib.h>
#include <string.h>
#include "arg.h"
#include "globals.h"
#include "help.h"
#include "log.h"

int arg_islocal(char *filename)
{
	return !!strchr(filename, '/');
}

int arg_parse(int *argc, char **argv[])
{
	int rc = 0;
	opt_t oquiet = { .oshort = 'q', .olong = "quiet", .var_bool = &quiet, .type = OTYPE_BOOL, };
	opt_t overbose = { .oshort = 'v', .olong = "verbose", .var_bool = &verbose, .type = OTYPE_BOOL, };
	opt_t ologlevel = { .olong = "loglevel", .var_int = &loglevel, .type = OTYPE_INT, };
	opt_t ostart = { .olong = "start", .var_long = &tstart, .type = OTYPE_INT, };
	opt_t oend = { .olong = "end", .var_long = &tend, .type = OTYPE_INT, };
	opt_t ogridh = { .olong = "gridh", .var_long = &gridh, .type = OTYPE_INT, };
	opt_t ogridv = { .olong = "gridv", .var_long = &gridv, .type = OTYPE_INT, };
	opt_t oxtime = { .olong = "xtime", .var_bool = &xtime, .type = OTYPE_BOOL, };
	opt_t oival = { .olong = "ival", .var_long = &tival, .type = OTYPE_INT, };
	opt_t oheight = { .olong = "height", .var_long = &height, .type = OTYPE_INT, };
	opt_t owidth = { .olong = "width", .var_long = &width, .type = OTYPE_INT, };
	opt_t oradius = { .olong = "radius", .var_long = &radius, .type = OTYPE_INT, };
	opt_t ofields = { .olong = "fields", .var_long = &fieldc, .type = OTYPE_INT, };
	opt_t otitle = { .olong = "title", .oshort = 't', .var_str = &title, .type = OTYPE_STR, };
	opt_t otitlecolor = { .olong = "titlecolor", .var_str = &titlecolor, .type = OTYPE_STR, };
	opt_t ogridcolor = { .olong = "gridcolor", .var_str = &gridcolor, .type = OTYPE_STR, };
	opt_t obgcolor = { .olong = "bgcolor", .var_str = &bgcolor, .type = OTYPE_STR, };
	opt_t obgcolorinner = { .olong = "bgcolor-inner", .var_str = &bgcolorinner, .type = OTYPE_STR, };
	opt_t ofgcolor = { .olong = "fgcolor", .var_str = &fgcolor, .type = OTYPE_STR, };
	opt_t obordercolor = { .olong = "bordercolor", .var_str = &bordercolor, .type = OTYPE_STR, };
	opt_t ocolor = { .olong = "color", .var_stra = &colorv, .type = OTYPE_STRA, .len = &colorc, };
	opt_t olabel = { .olong = "label", .var_stra = &labelv, .type = OTYPE_STRA, .len = &labelc, };
	opt_t ounit = { .olong = "unit", .var_stra = &unitv, .type = OTYPE_STRA, .len = &unitc, };
	opt_t oscale = { .olong = "scale", .var_stra = &scalev, .type = OTYPE_STRA, .len = &scalec, };
	opt_t omax = { .olong = "max", .var_stra = &ymaxv, .type = OTYPE_STRA, .len = &ymaxc, };
	opt_t oxmax = { .olong = "xmax", .var_float = &x_max, .type = OTYPE_FLOAT, };
	opt_t oout = { .olong = "out", .oshort = 'o', .var_str = &outfile, .type = OTYPE_STR, };
	opt_t ocolordrop = { .olong = "color-drop", .var_str = &color_drop, .type = OTYPE_STR, };
	opt_t ocolorfail = { .olong = "color-fail", .var_str = &color_fail, .type = OTYPE_STR, };
	opt_t ocolornull = { .olong = "color-null", .var_str = &color_null, .type = OTYPE_STR, };
	opt_t ocolormissing = { .olong = "color-missing", .var_str = &color_missing, .type = OTYPE_STR, };
	opt_t oplot = { .olong = "plot", .var_stra = &plotv, .type = OTYPE_STRA, .len = &plotc, };
	parser = opt_init(32);
	opt_new(parser, &oquiet);
	opt_new(parser, &overbose);
	opt_new(parser, &oxtime);
	opt_new(parser, &ologlevel);
	opt_new(parser, &otitle);
	opt_new(parser, &otitlecolor);
	opt_new(parser, &ogridcolor);
	opt_new(parser, &obgcolor);
	opt_new(parser, &obgcolorinner);
	opt_new(parser, &ofgcolor);
	opt_new(parser, &obordercolor);
	opt_new(parser, &ocolor);
	opt_new(parser, &ogridh);
	opt_new(parser, &ogridv);
	opt_new(parser, &olabel);
	opt_new(parser, &ounit);
	opt_new(parser, &oscale);
	opt_new(parser, &omax);
	opt_new(parser, &oxmax);
	opt_new(parser, &ostart);
	opt_new(parser, &oend);
	opt_new(parser, &oival);
	opt_new(parser, &oheight);
	opt_new(parser, &owidth);
	opt_new(parser, &oradius);
	opt_new(parser, &ofields);
	opt_new(parser, &oout);
	opt_new(parser, &ocolordrop);
	opt_new(parser, &ocolorfail);
	opt_new(parser, &ocolormissing);
	opt_new(parser, &ocolornull);
	opt_new(parser, &oplot);
	rc = opt_parse(parser, argc, argv);
	if (verbose) {
		loglevel = LOG_LOGLEVEL_VERBOSE;
	}
	if (quiet) {
		loglevel = LOG_LOGLEVEL_QUIET;
	}
	if (rc) help_usage();
	return rc;
}
