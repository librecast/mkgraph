# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only
# Copyright (c) 2017-2022 Brett Sheffield <bacs@librecast.net>

OSNAME := $(shell uname -s)
export OSNAME
#CC=clang
CFLAGS += -g -Wall -Wextra -Wpedantic
ifeq ($(OSNAME),Linux)
#CFLAGS += -std=c99 -D_POSIX_C_SOURCE=200112L
else
	MAKE := gmake
endif
export CFLAGS
#LDFLAGS += -pthread
#export LDFLAGS
PROGRAM := mkgraph

.PHONY: common src clean realclean install

all: common src

src common:
	$(MAKE) -C $@

clean realclean:
	$(MAKE) -C src $@
	$(MAKE) -C common $@

install:
	$(MAKE) -C src $@
	$(MAKE) -C doc $@
